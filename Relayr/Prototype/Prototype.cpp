// Prototype.cpp : Defines the entry point for the console application.
// UDP based server for Tick Tac Toe game

#include "stdafx.h"
#include <csignal>
#include <iostream>

// Control-C interrupt handler
BOOL WINAPI interrupt_handler(_In_ DWORD  dwCtrlType)
{
	ExitProcess(0);
}

// filename ip port
int main(int argc, char *argv[])
{
	SetConsoleCtrlHandler(interrupt_handler,TRUE);
	int err;
	InputControl datain;	// Used to validate argv input

	ComUDP Com;				// Provides UDP socket communication
	MsgHandler Msg;			// Provides msg serialization/deserialization
	TickTacToe Game;		// Actual game - handles board and player moves
	Core appCore;			// Main object - interprets and executes requests, selects correct responses
	
	// Check WinSock initialization
	if (err = WSAGetLastError())
	{
		std::cout << "WinSock Error ocurred: " << err << std::endl;
		system("pause");
		return -1;
	}

	// Check if argv containt targets ip and port number
	if (datain.proccessInput(argc, argv))
	{
		// Use provided ip and port
		if(Com.setIp(datain.getIP()) == 1) std::cout << "Ip set: " << datain.getIP() << std::endl;
		else
		{
			// Ip conversion fail
			std::cout << "Ip set error." << std::endl;
			system("pause");
			return -2;
		}
		Com.setPort(datain.getPort());
		std::cout << "Port set: " << datain.getPort() << std::endl;
	}
	// ip and/or port not specified or invalid
	else
	{
		// Use fallback ip and port
		if (Com.setIp(DEFAULT_IP) == 1) std::cout << "Ip set: " << DEFAULT_IP << std::endl;
		else
		{
			// Ip conversion fail
			std::cout << "Ip set error." << std::endl;
			system("pause");
			return -2;
		}
		Com.setPort(DEFAULT_PORT);
		std::cout << "Port set: " << DEFAULT_PORT << std::endl;
	}
	
	// Prepare Com for operation bind socket
	Com.ready();

	// Verify Winsock state
	if (err = WSAGetLastError())
	{
		std::cout << "WinSock Error ocurred: " << err << std::endl;
		system("pause");
		return -1;
	}

	// Pass components to core
	appCore.setCom(&Com);
	appCore.setMessenger(&Msg);
	appCore.setGame(&Game);
	
	// Start main loop, Core::startGame() should not return 
	if (appCore.startGame()) std::cout << "Error ocurred" << std::endl;
	system("pause");
    return 1;
}

