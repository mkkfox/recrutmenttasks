// TestClient.cpp : Defines the entry point for the console application.
// Simplified UDP based TicTacToe client designed for quick server testing 
// User input potentially unsafe
// Time out not provided

#include "stdafx.h"
#include <iostream>

// filename ip port
int main(int argc, char *argv[])
{
	ClientUDP Com; // Provides socket communication (UDP)
	MsgBuilder mb; // Provides request messages

	SOCKADDR *serveraddr;
	std::string msg;
	json j;
	InputControl datain;
	int err;

	// Check WinSock initialization
	if (err = WSAGetLastError())
	{
		std::cout << "WinSock Error occured: " << err << std::endl;
		system("pause");
		return 1;
	}

	// Check if argv containt targets ip and port number
	if (datain.proccessInput(argc, argv))
	{
		// Use provided ip and port
		serveraddr = Com.getAddr(datain.getIP(), datain.getPort());
		std::cout << "Server ip set: " << datain.getIP() << std::endl;
		std::cout << "Server port set: " << datain.getPort() << std::endl;
	}
	// ip and/or port not specified or invalid
	else 
	{
		// Use fallback ip and port
		serveraddr = Com.getAddr(DEFAULT_IP, DEFAULT_PORT);
		std::cout << "Server ip set: " << DEFAULT_IP << std::endl;
		std::cout << "Server port set: " << DEFAULT_PORT << std::endl;
	}

	// Connect socket - allows to use Com::msgSend
	Com.connectTo(serveraddr);

	// Verify Winsock state
	if (err = WSAGetLastError())
	{
		std::cout << "Connect failed " << err << std::endl;
		system("pause");
		return 2;
	}

	// Main client loop
	for (char input;;)
	{
		std::cout << "1. Request move \n2. Request State\n0. Exit\n\n";
		std::cin.clear();
		std::cin.sync();

		// Get user input - unsafe
		std::cin >> input;
		switch (input)
		{
		case '1':
			msg = mb.buildRequestMove();		// Build move request msg
			msg = Com.msgSend(msg);				// Send request, wait for response // No timeout
			j = mb.deserialize(msg);			// Deserialize msg, convert to json object
			std::cout << j.dump(5) << std::endl;// Print msg to std::cout
			break;
		case '2':
			msg = mb.buildRequestState();		// Build state request msg
			msg = Com.msgSend(msg);				// Send request, wait for response // No timeout
			j = mb.deserialize(msg);			// Deserialize msg, convert to json object
			std::cout << j.dump(5) << std::endl;// Print msg to std::cout
			break;
		case '0':
			exit(0);							// Exit application
		default:
			
			std::cout << "unknown command\n\n";	// Unregistered command
		}
	}
	
    return 0;
}

