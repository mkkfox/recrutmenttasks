#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Relayr/MsgHandler.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{
	TEST_CLASS(MsgTest)
	{
		MsgHandler testobject;
		MsgHandlerInterface *Msg = &testobject;
		json j;
		std::string s;
	public:
		
		TEST_METHOD(MsgSerialize1)
		{
			s = Msg->toMsg();
			Assert::AreEqual("{\"response\":\"ok\"}", s.c_str());
			s.clear();
		}

		TEST_METHOD(MsgSerialize2) 
		{
			s = Msg->toMsg(1);
			Assert::AreEqual("{\"error_code\":1,\"response\":\"error\"}", s.c_str());
			s.clear();
		}

		TEST_METHOD(MsgSerialize3)
		{
			char * m = new char[10];
			m = "AB.AAA..B";
			s = Msg->toMsg(m);
			Assert::AreEqual("{\"board\":[\"AB.\",\"AAA\",\"..B\"],\"response\":\"game status\"}", s.c_str());
			s.clear();
		}

		TEST_METHOD(MsgSerialize4)
		{
			s = Msg->toMsg(std::string("A won"));
			Assert::AreEqual("{\"details\":\"A won\",\"response\":\"game over\"}", s.c_str());
			s.clear();
		}

		TEST_METHOD(MsgDeserialize1)
		{
			s = "{\"request\": \"player move\",\"player\" : \"A\",\"position\" : {\"x\": 2, \"y\" : 1}}";
			j = Msg->toRequest(s);
			Assert::IsTrue(j["request"].get<std::string>() == "player move");
			s.clear();
			j.clear();
		}
		TEST_METHOD(MsgDeserialize2)
		{
			s = "{\"request\": \"player move\",\"player\" : \"A\",\"position\" : {\"x\": 2, \"y\" : 1}}";
			j = Msg->toRequest(s);
			Assert::IsTrue(j["position"]["x"] == 2);
			j.clear();
			s.clear();
		}
		TEST_METHOD(MsgDeserialize3)
		{
			s = "{\"request\": \"player move\",\"player\" : \"A\",\"position\" : {\"x\": 2, \"y\" : 1}}";
			j = Msg->toRequest(s);
			Assert::IsTrue(j.count("position"));
			j.clear();
			s.clear();
		}
		TEST_METHOD(MsgDeserialize4)
		{
			s = "{\"request\": \"game status\"}";
			j = Msg->toRequest(s);
			Assert::IsTrue(j["request"].get<std::string>() == "game status");
			j.clear();
			s.clear();
		}
	};
}