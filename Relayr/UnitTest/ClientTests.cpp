#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Relayr/ClientUDP.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{		
	TEST_CLASS(ClientUDPtest)
	{
	public:
		

		TEST_METHOD(ClientWinSocInit)
		{
			ClientInterface *TestClient = new ClientUDP();
			Assert::IsFalse(WSAGetLastError());
			delete TestClient;
		}
	};
}
/*
	// Send msg to connected server
	virtual std::string msgSend(std::string msg) override;

	// Send msg to specified server
	virtual std::string msgSendTo(std::string msg, sockaddr * serverAddr) override;

	// Connect to specified server
	virtual int connectTo(const sockaddr *serverAddr) override;

	// Close current socket and create new socket
	virtual int disconect() override;

	// Get connected server sockaddr
	virtual sockaddr getConnectedServerAddr() const override;

	// Get Addr
	virtual sockaddr * getAddr(const char* ip, const int port) override;
*/