#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Relayr/GameDefines.h"
#include "../Relayr/TickTacToe.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{
	TEST_CLASS(GameTest)
	{
		TickTacToe testobject;
		GameInterface *Game = &testobject;
		PlayerMove testmove;
		int res;
	public:
		TEST_METHOD(GameMoveInvalidPlayer)
		{
			testmove = { 'x',(unsigned int)0,(unsigned int)0 };
			res = Game->makeMove(testmove);
			Assert::AreEqual(INVALID_PLAYER, res);
		}

		TEST_METHOD(GameValidMove1A)
		{
			testmove = { 'A',(unsigned int)0,(unsigned int)0 };
			res = Game->makeMove(testmove);
			Assert::AreEqual(INPROGRESS, res);
		}

		TEST_METHOD(GameValidMove1B)
		{
			testmove = { 'B',(unsigned int)0,(unsigned int)0 };
			res = Game->makeMove(testmove);
			Assert::AreEqual(INVALID_PLAYER, res);
		}

		TEST_METHOD(GameValidMove2A)
		{
			testmove = { 'A',(unsigned int)0,(unsigned int)0 };
			res = Game->makeMove(testmove);
			Assert::AreEqual(INPROGRESS, res);
			testmove = { 'B',(unsigned int)2,(unsigned int)2 };
			res = Game->makeMove(testmove);
			Assert::AreEqual(INPROGRESS, res);
		}

		TEST_METHOD(GameValidMove2B)
		{
			testmove = { 'B',(unsigned int)0,(unsigned int)0 };
			res = Game->makeMove(testmove);
			Assert::AreEqual(INVALID_PLAYER, res);
			testmove = { 'A',(unsigned int)0,(unsigned int)0 };
			res = Game->makeMove(testmove);
			Assert::AreEqual(INPROGRESS, res);
		}

		TEST_METHOD(GameMoveOutofRange)
		{
			testmove = { 'A',(unsigned int)3,(unsigned int)0 };
			res = Game->makeMove(testmove);
			if (res != OUT_OF_RANGE) Assert::Fail();
			testmove = { 'A',(unsigned int)0,(unsigned int)-2 };
			res = Game->makeMove(testmove);
			Assert::AreEqual(OUT_OF_RANGE, res);
		}

		TEST_METHOD(GameAllreadyOcupied)
		{
			testmove = { 'A',(unsigned int)0,(unsigned int)0 };
			res = Game->makeMove(testmove);
			Assert::AreEqual(INPROGRESS, res);
			testmove = { 'B',(unsigned int)0,(unsigned int)0 };
			res = Game->makeMove(testmove);
			Assert::AreEqual(ALREADY_OCCUPIED, res);
		}

		TEST_METHOD(GameMoveWrongPlayer)
		{
			testmove = { 'A',(unsigned int)0,(unsigned int)0 };
			res = Game->makeMove(testmove);
			Assert::AreEqual(INPROGRESS, res);
			testmove = { 'A',(unsigned int)0,(unsigned int)0 };
			res = Game->makeMove(testmove);
			Assert::AreEqual(INVALID_PLAYER, res);
		}
		
		TEST_METHOD(GamePlayerAwin)
		{
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)0, (unsigned int)0 })); // A A A
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)1, (unsigned int)0 })); // B . .
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)0, (unsigned int)1 })); // B . .
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)2, (unsigned int)0 }));
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)0, (unsigned int)2 }));
			Assert::AreEqual(PLAYER1_WON, res);
		}

		TEST_METHOD(GamePlayerBwin)
		{
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)1, (unsigned int)1 })); // B B B
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)0, (unsigned int)0 })); // . A .
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)2, (unsigned int)0 })); // A . A
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)0, (unsigned int)2 }));
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)2, (unsigned int)2 }));
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)0, (unsigned int)1 }));
			Assert::AreEqual(PLAYER2_WON, res);
		}

		TEST_METHOD(GameUndecided)
		{
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)1, (unsigned int)1 }));
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)0, (unsigned int)0 })); // B A B
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)2, (unsigned int)0 }));	// A A B
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)0, (unsigned int)2 }));	// A B A
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)0, (unsigned int)1 }));
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)2, (unsigned int)1 }));
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)2, (unsigned int)2 }));
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)1, (unsigned int)2 }));
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)1, (unsigned int)0 }));
			Assert::AreEqual(UNDECIDED, res);
		}
		TEST_METHOD(GameFullBoardAwin)
		{
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)1, (unsigned int)1 }));
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)0, (unsigned int)0 })); // B A B
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)2, (unsigned int)0 }));	// A A B
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)2, (unsigned int)2 }));	// A B A
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)0, (unsigned int)1 }));
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)2, (unsigned int)1 }));
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)1, (unsigned int)0 }));
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)1, (unsigned int)2 }));
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)0, (unsigned int)2 }));
			Assert::AreEqual(PLAYER1_WON, res);
		}

		TEST_METHOD(GameReset)
		{
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)1, (unsigned int)1 }));
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)0, (unsigned int)0 })); // B A B
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)2, (unsigned int)0 }));	// A A B
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)0, (unsigned int)2 }));	// A B A
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)0, (unsigned int)1 }));
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)2, (unsigned int)1 }));
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)2, (unsigned int)2 }));
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)1, (unsigned int)2 }));
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)1, (unsigned int)0 }));
			Assert::AreEqual(UNDECIDED, res);
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)0, (unsigned int)0 }));
			Assert::AreEqual(INPROGRESS, res);
		}

		TEST_METHOD(GetGameState)
		{
			char *gb;
			gb = Game->getGameState();
			Assert::AreEqual(".........", gb);
			delete[] gb;
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)1, (unsigned int)1 }));
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)0, (unsigned int)0 })); // B A B
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)2, (unsigned int)0 }));	// A A B
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)0, (unsigned int)2 }));	// A B A
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)0, (unsigned int)1 }));
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)2, (unsigned int)1 }));
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)2, (unsigned int)2 }));
			res = Game->makeMove(PlayerMove({ 'B',(unsigned int)1, (unsigned int)2 }));
			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)1, (unsigned int)0 }));
			gb = Game->getGameState();
			Assert::AreEqual("BABAABABA", gb);
			delete[] gb;

			res = Game->makeMove(PlayerMove({ 'A',(unsigned int)0, (unsigned int)0 }));
			gb = Game->getGameState();
			Assert::AreEqual("A........", gb);
			delete[] gb;
		}
	};
}
