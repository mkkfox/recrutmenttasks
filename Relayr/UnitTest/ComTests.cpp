#include "stdafx.h"
#include "CppUnitTest.h"
#include"../Relayr/ComUDP.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{
	TEST_CLASS(ComUDPtest)
	{
		ComUDP testobject;
		ComInterface *Com = &testobject;
	public:
		TEST_METHOD(ComWinSocInit)
		{
			Assert::IsFalse(WSAGetLastError());
		}

		TEST_METHOD(ComSetIP)
		{
			int result = Com->setIp("192.168.1.1");
			if (!result) Assert::Fail();
			Assert::IsFalse(WSAGetLastError());
		}

		TEST_METHOD(ComSetPort)
		{
			u_short result = Com->setPort(2020);
			Assert::AreEqual((int)htons(2020), (int)result);
		}

		TEST_METHOD(ComReady)
		{
			Com->ready();
			Assert::IsFalse(WSAGetLastError());
		}
	};
}
// Not tested
/*	
	// Wait for new message (blocking)
	virtual std::string getMsg();

	// Reply to last message
	virtual int reply(const std::string msg);
*/