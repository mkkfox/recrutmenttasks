#include "ComUDP.h"

int ComUDP::construct(const char ip[], const int port)
{
	ComUDP::initialize();
	ComUDP::createSocket();
	ComUDP::preperAddr();
	ComUDP::setIp(ip);
	ComUDP::setPort(port);
	return 0;
}

ComUDP::ComUDP()
{
	construct(DEFAULT_IP, DEFAULT_PORT);
}

ComUDP::ComUDP(const int port) 
{
	construct(DEFAULT_IP, port);
}

ComUDP::ComUDP(const char ip[])
{
	construct(ip, DEFAULT_PORT);
}

ComUDP::ComUDP(const char ip[], const int port)
{
	construct(ip, port);
}

ComUDP::~ComUDP()
{
	cleanup();
}

int ComUDP::initialize()
{
	UDPSocket = INVALID_SOCKET;

	int err;
	if (err = WSAStartup(MAKEWORD(2, 2), &wsaData))
	{
		printf("WSAStartup error: %d\n", err);
		return -1;
	}
	return 0;
}

int ComUDP::createSocket()
{

	UDPSocket = socket(AF_INET, SOCK_DGRAM, 0);;
	if (UDPSocket == INVALID_SOCKET)
	{
		printf("Error creating socket: %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	return 0;
}

int ComUDP::preperAddr()
{
	ZeroMemory(&serverAddr, sizeof(serverAddr));
	ZeroMemory(&senderAddr, sizeof(senderAddr));
	serverAddr.sin_family = AF_INET;
	addrSize = sizeof(senderAddr);
	return 0;
}

int ComUDP::bindSocket()
{
	if (bind(UDPSocket, (sockaddr*)&serverAddr, sizeof(serverAddr)) == SOCKET_ERROR)
	{
		printf("bind() failed.\n");
		closesocket(UDPSocket);
		return 1;
	}
	return 0;
}

int ComUDP::cleanup()
{
	closesocket(UDPSocket);
	WSACleanup();
	return 0;
}

int ComUDP::setIp(const char * ip)
{
	return InetPton(AF_INET, ip, &serverAddr.sin_addr);
}

u_short ComUDP::setPort(const int port)
{
	return serverAddr.sin_port = htons(port);
}

int ComUDP::ready()
{
	return bindSocket();
}
// TODO: Add error control
std::string ComUDP::getMsg()// TODO: Add error control
{
	char received[BUFF_SIZE];
	int bytes;
	bytes = recvfrom(UDPSocket, received, BUFF_SIZE,0, &senderAddr,&addrSize);
	received[bytes] = '\0';
	return std::string(received); // NULL will throw
}
// TODO: Add error control
int ComUDP::reply(const std::string msg)	
{
	sendto(UDPSocket, msg.c_str(), msg.size(), 0, &senderAddr, addrSize);
	return 0;
}
