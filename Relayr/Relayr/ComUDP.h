#pragma once
// Basic Com class for server apps provides two way UDP socket based communication 

#include "ComInterface.h"

class ComUDP :
	public ComInterface
{
int construct(const char ip[], const int port);		// Make object
public:
	ComUDP();
	ComUDP(const int port);
	ComUDP(const char ip[]);
	ComUDP(const char ip[], const int port);
	virtual ~ComUDP();

protected:
	SOCKET UDPSocket;		// Socket descriptor
	sockaddr_in serverAddr;	// Server info
	sockaddr senderAddr;	// Sender info
	int addrSize;			// senderAddr size
	WSADATA wsaData;		// Winsock info

private:
	int initialize();		// Initialize WinSock
	int createSocket();		// Create socket
	int preperAddr();		// Specify sockaddr
	int bindSocket();		// Bind socket
	int cleanup();			// Cleanup all

public:
	// Set server IP
	virtual int setIp(const char *ip) override;

	// Set server port
	virtual u_short setPort(const int port) override;

	// Prepare for operation
	virtual int ready() override;

	// Wait for new message (blocking)
	virtual std::string getMsg() override;

	// Reply to last message
	virtual int reply(const std::string msg) override;
};

