#pragma once
// Core - main application class, provides server main loop.
// Can be used with any ComInterface and MsgHandlerInterface implementations.
// Analyzes deserialized messenges and executes requests.
// Should work with any GameInterface implemention.

#include <iostream>
#include "ComInterface.h"
#include "GameInterface.h"
#include "MsgHandlerInterface.h"
#include "GameDefines.h"

class Core
{
public:
	Core();
	Core(ComInterface *Com, GameInterface *Game, MsgHandlerInterface *Messenger);
	~Core();
private:
	ComInterface *Com;			// Provides communication
	MsgHandlerInterface *Msg;	// Provides msg serialization/deserialization
	GameInterface *Game;		// Actual game - handles board and player moves

	std::string msg;			// Received serialized msg
	std::string response;		// Serialized response 
	json request;				// Json object - deserialized msg
	int result;					// Used to store analyse functions results 
	char* boardState;			// Used to store C_str returned by GameInterface::getGameState()

	// Deserialize and analyze request, make move or get state
	int analyseRequest();

	// Analyze response, prepere replay
	int analyseResponse();

public:
	// Set Com object must implement ComInterface and be initialized
	void setCom(ComInterface *Com);
	ComInterface * getCom();

	// Set Game object must implement GameInterface and be initialized
	void setGame(GameInterface *Game);
	GameInterface* getGame();

	// Set Msg object must implement MsgHandlerInterface and be initialized
	void setMessenger(MsgHandlerInterface *Messenger);
	MsgHandlerInterface * getMessenger();
	
	// Infinite loop, returns only if error occurs
	int startGame();
};

