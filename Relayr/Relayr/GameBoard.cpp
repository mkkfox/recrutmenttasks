#include "GameBoard.h"

GameBoard::GameBoard()
{
	arbiter.setTarget(&board);
}

GameBoard::GameBoard(char init)
{
	clearBoard(init);
	arbiter.setTarget(&board);
}

GameBoard::~GameBoard()
{
}

bool GameBoard::checkRange(unsigned int row, unsigned int column)
{
	if ((row < 3) && (column < 3)) return 0;
	return 1;
}

int GameBoard::set(char val, unsigned int row, unsigned int column)
{
	if (checkRange(row, column)) return OUT_OF_RANGE;
	if (board.x[row][column] != '.') return ALREADY_OCCUPIED;
	board.x[row][column] = val;
	return 0;
}

int GameBoard::set(PlayerMove move)
{
	return set(move.playerId, move.row, move.column);
}

char GameBoard::get(unsigned int row, unsigned int column)
{
	if (checkRange(row, column)) return (char)'/0';
	return board.x[row][column];
}

char * GameBoard::getBoard()
{
	char *present = new char[10];
	for (int i = 0; i < 9; i++)
		present[i] = board.x[(i - (i % 3))/3][i % 3];
	present[9] = '\0';
	return present;
}

void GameBoard::clearBoard(char val)
{
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			board.x[i][j] = val;
}
