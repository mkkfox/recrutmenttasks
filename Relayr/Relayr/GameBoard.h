#pragma once
// Simple game board handling class, provides range checking on each operation.
// getGoard() provides pointer to heap allocated c_str with game board content.

#include "Evaluator.h"
#include "GameDefines.h"

class GameBoard
{
public:
	GameBoard();
	GameBoard(char init);
	~GameBoard();
	Evaluator arbiter;	// Provieds victory conditions check
protected:
	Board3x3 board;
	bool checkRange(unsigned int row,unsigned int column);	// verify if row and column don't exceed range
public:
	// Set value at (row, column) 
	int set(char val, unsigned int row, unsigned int column);
	int set(PlayerMove move);
	
	// Get value from (row, column)
	char get(unsigned int row, unsigned int column);

	// Get board state as C-string (row1row2row3) don't forget to delete[]
	char* getBoard();

	// Fill board with given char
	void clearBoard(char val);
};

