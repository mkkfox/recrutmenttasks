#pragma once
// Defines interface for serialization/deserialization classes for server apps.
// toMsg should provied correct serialized response msg based on argument type.
// toRequest should parse/deserialize msg to JSON object.

#include <string>
#include"../JSON/json.hpp"

// for convenience
using json = nlohmann::json;

class MsgHandlerInterface
{
public:
	MsgHandlerInterface();
	virtual ~MsgHandlerInterface();

	// Serialize {"response": "ok"} 
	virtual std::string toMsg() = 0;

	// Serialize "response": "error", "error_code": err
	virtual std::string toMsg(int err) = 0;

	// Serialize "response": "game status", "board": ["row1", "row2", "row3"]
	virtual std::string toMsg(char* gameBoard) = 0;

	// Serialize "response": "game over", "details": winner
	virtual std::string toMsg(std::string winner) = 0;

	// Parse msg to JSON object (deserialize)
	virtual json toRequest(std::string msg) = 0;
};

