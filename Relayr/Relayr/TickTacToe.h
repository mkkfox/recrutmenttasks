#pragma once
// Tick Tac Toe game implements GameInterface.
// Controls game state, tracts players' turns.

#include "GameBoard.h"
#include "GameInterface.h"
#include "GameDefines.h"

class TickTacToe:
	public GameInterface
{
public:
	TickTacToe();
	~TickTacToe();
private:
	char playerturn;				// Current turn's player identifier
	int movesLeft;					// Moves left in current game instance
	GameBoard board;				// Game board and player move handling class

	int checkVictory();				// Check if game is still inprogress / get winner
	int verifyMove(PlayerMove move);// Verify player validity
	int restart();					// Restart game
public:
	// Request player's move
	int makeMove(PlayerMove move) override;

	// Get current game status don't forget to delete[]
	char* getGameState() override;

};

