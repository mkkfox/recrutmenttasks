#pragma once
// Message serialization/deserialization for client apps.
// Builds move requests from std::cin.
// Predefined state request build sequence.

#include <iostream>
#include "../JSON/json.hpp"

using json = nlohmann::json;

class MsgBuilder
{
	
public:
	MsgBuilder();
	~MsgBuilder();
private:
	json msg;					// Json for msg storing
	void msgClear();			// Clear msg
	void streamSync();			// Clear std::cin stream buffor and error flags
	std::string serialize();	// Serialize msg
public:
	// Build and serialize move request msg from user imput
	std::string buildRequestMove();

	// Build serialized game status request
	std::string buildRequestState();

	// Get serialized json msg
	std::string getLastMsg();

	// Get json object (deserialize) from str
	json deserialize(std::string str);
};

