#include "Evaluator.h"

Evaluator::Evaluator()
{
	target = nullptr;
}

Evaluator::Evaluator(Board3x3 * board)
{
	setTarget(board);
}

Evaluator::~Evaluator()
{
	target = nullptr;
	lines.clear();
}

int Evaluator::build()
{
	if (target == nullptr) return -1;
	lines.clear();
	WinLine temp;
	for (int i = 0; i < 3; i++)
	{
		// Horizontal
		temp = { &target->x[i][0],&target->x[i][1],&target->x[i][2] };
		lines.push_back(temp);	
		// Vertical
		temp = { &target->x[0][i],&target->x[1][i],&target->x[2][i] };
		lines.push_back(temp);	
	}
	// Diagonal
	temp = { &target->x[0][0],&target->x[1][1],&target->x[2][2] };
	lines.push_back(temp);
	temp = { &target->x[0][2],&target->x[1][1],&target->x[2][0] };
	lines.push_back(temp);
	return 0;
}

int Evaluator::setTarget(Board3x3 * board)
{
	if (board == nullptr) return -1;
	target = board;
	return build();
}

char Evaluator::check()
{
	for (WinLine line : lines)
	{
		if ((*line.f1 == '.') || (*line.f2 == '.') || (*line.f3 == '.')) continue;
		else if ((*line.f1 == *line.f2) && (*line.f2 == *line.f3)) return *line.f1;
	}
	return 0;
}
