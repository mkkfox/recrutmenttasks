#include "InputControl.h"



InputControl::InputControl()
{
	IP = nullptr;
	port = nullptr;
}

InputControl::~InputControl()
{
	IP =nullptr;
	delete port;
}

bool InputControl::proccessInput(int argc, char * argv[])
{
	sockaddr_in temp;
	if (argc < 3)return false;
	port = new int;
	std::cout << std::endl << "Input:\nip: " << argv[1] << "\nPort: " << argv[2] << std::endl;
	if (!(*port = atoi(argv[2])))
	{
		std::cout << "\nError port: nan\n\n";
		delete port;
		port = nullptr;
		return false;
	}
	if (1 != InetPton(AF_INET, argv[1], &temp.sin_addr))
	{
		std::cout << "\nError ip: bad format\n\n";
		return false;
	}
	IP = argv[1];
	return true;
}

char* InputControl::getIP()
{
	return IP;
}

int InputControl::getPort()
{
	if (port == nullptr) return NULL;
	return *port;
}
