#include "Core.h"

Core::Core()
{
	Com = nullptr;
	Game = nullptr;
	Msg = nullptr;
}

Core::Core(ComInterface * Com, GameInterface * Game, MsgHandlerInterface * Messenger)
{
	setCom(Com);
	setGame(Game);
	setMessenger(Messenger);
}

Core::~Core()
{
	delete Msg;
	Msg = nullptr;
	delete Game;
	Game = nullptr;
	delete Com;
	Com = nullptr;
}

int Core::analyseRequest()
{
	if (request.count("request"))
	{
		std::string value = request["request"];
		if (!value.compare("player move"))
		{
			PlayerMove move;
			try
			{
				move.playerId = request["player"].get<char>();
				move.column = request["position"]["x"].get<unsigned int>();
				move.row = request["position"]["y"].get<unsigned int>();
			}
			catch (json::type_error& ex)
			{
				std::cout << "Json error: " + ex.id;
				request.clear();
				return INVALID_REQUEST;
			}
			return Game->makeMove(move);
		}else
		if (!value.compare("game status"))
		{
			boardState = Game->getGameState();
			return STATE_REQUESTED;
		}
		else return INVALID_REQUEST;
	}

	return 0;
}

int Core::analyseResponse()
{
	switch (result)
	{
	case STATE_REQUESTED:
		response = Msg->toMsg(boardState);
		delete[] boardState;
		break;
	case INPROGRESS:
		response = Msg->toMsg();
		break;
	case  PLAYER1_WON:
		response = Msg->toMsg(std::string("A won"));
		break;
	case PLAYER2_WON:
		response = Msg->toMsg(std::string("B won"));
		break;
	case UNDECIDED:
		response = Msg->toMsg(std::string("undecided"));
		break;
	default: //INVALID_REQUEST, INVALID_PLAYER, OUT_OF_RANGE, ALREADY_OCCUPIED
		response = Msg->toMsg(result);
	}
	return 0;
}

void Core::setCom(ComInterface * Com)
{
	std::cout << "Com set" << std::endl;
	this->Com = Com;
}

ComInterface * Core::getCom()
{
	return Com;
}

void Core::setGame(GameInterface * Game)
{
	std::cout << "Game set" << std::endl;
	this->Game = Game;
}

GameInterface * Core::getGame()
{
	return Game;
}

void Core::setMessenger(MsgHandlerInterface * Messenger)
{
	std::cout << "Messenger set" << std::endl;
	this->Msg = Messenger;
}

MsgHandlerInterface * Core::getMessenger()
{
	return Msg;
}

int Core::startGame()
{
	if ((Game == nullptr) || (Com == nullptr) || (Msg == nullptr)) return -101;
	std::cout << "Main loop started press CTRL + C to kill" << std::endl;
	while (1)
	{
		// Get new msg
		msg = Com->getMsg();
		std::cout << "Msg recived: " << std::endl;
		// Deserialize msg
		request = Msg->toRequest(msg);
		std::cout << request.dump(5) << std::endl;
		// make move | check board | deny request
		result = analyseRequest();
		// Prepare response & Serialize msg
		result = analyseResponse();
		std::cout << "Server response: " << response << std::endl;
		// respond to sender
		Com->reply(response);
	}
}
