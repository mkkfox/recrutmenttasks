#pragma once
// Provides validation of input arguments passed by (int argc, char *argv[]).
// Checks whether argv[1] is dotted style IP address and argv[2] is valid port number.
// Provides direct access to valid values.

#include <iostream>
#include <string>
#include <WinSock2.h>
#include <Ws2tcpip.h>

class InputControl
{
public:
	InputControl();
	~InputControl();
	
	// filename ip port
	bool proccessInput(int argc, char *argv[]);

	// Recover c_str ip 
	char* getIP();	

	// Recover port number
	int getPort();
private:
	char *IP;	// Ptr to c_str dot-decimal IP
	int *port;	// Ptr to stored port number
};

