#include "MsgBuilder.h"

MsgBuilder::MsgBuilder()
{
}

MsgBuilder::~MsgBuilder()
{
}

void MsgBuilder::msgClear()
{
	msg.clear();
}

void MsgBuilder::streamSync()
{
	std::cin.clear();
	std::cin.sync();
}

std::string MsgBuilder::serialize()
{
	return msg.dump();
}

// TODO: add imput control
std::string MsgBuilder::buildRequestMove()
{
	streamSync();
	msgClear();
	char id;
	unsigned int row, column;

	std::cout << "Player: ";
	std::cin >> id;
	std::cout << "Row: ";
	std::cin >> row;
	std::cout << "Column: ";
	std::cin >> column;

	msg["request"] = "player move";
	msg["player"] = id;
	msg["position"] = { {"x", column}, {"y", row} };

	return serialize();
}

std::string MsgBuilder::buildRequestState()
{
	msgClear();
	msg["request"] = "game status";
	return serialize();
}

std::string MsgBuilder::getLastMsg()
{
	return serialize();
}

json MsgBuilder::deserialize(std::string str)
{
	return json::parse(str);
}
