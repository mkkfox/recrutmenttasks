#pragma once
// Defines basic communication class interface for server applications.
// ComInterface::ready() should prepere socket for recv and/or recvfrom call. 
// If ip and port were not specified DEFAULT_IP and DEFAULT_PORT should be used.

#include <string>
#include <WinSock2.h>
#include <Ws2tcpip.h>
#include "NetworkDefines.h"

class ComInterface
{
public:
	ComInterface();
	virtual ~ComInterface();

	// Set server IP
	virtual int setIp(const char *ip) = 0;;

	// Set server port
	virtual u_short setPort(const int) = 0;

	// Prepare for operation
	virtual int ready() = 0;

	// Wait for new message (blocking)
	virtual std::string getMsg() = 0;

	// Reply to last message
	virtual int reply(const std::string msg) = 0;
};

