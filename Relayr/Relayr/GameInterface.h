#pragma once
// Define game interface, getGameState should return c_str representing current board state
// makeMove should return infomation about game state/move validity returned values are defined in GameDefines.h

#include "GameDefines.h"
class GameInterface
{
public:
	GameInterface();
	virtual ~GameInterface();

	// Request player's move - put playerID at row, column
	virtual int makeMove(PlayerMove move) = 0;
	// Get current game status - board values
	virtual char* getGameState() = 0;
};

