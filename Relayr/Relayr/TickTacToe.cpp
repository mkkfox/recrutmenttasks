#include "TickTacToe.h"

TickTacToe::TickTacToe()
{
	restart();
}

TickTacToe::~TickTacToe()
{
}

int TickTacToe::checkVictory() 
{
	char result = board.arbiter.check();
	switch (result)
	{
	case 'A':
		return PLAYER1_WON;
	case 'B':
		return PLAYER2_WON;
	default:
		if (movesLeft)return INPROGRESS;
	}
	return UNDECIDED;
}

int TickTacToe::verifyMove(PlayerMove move)
{
	if (movesLeft == 0) restart();
	// Any player can start unused since only player A should be able to begin
	/*
	if (movesLeft == 9) if ((move.playerId != 'A') && (move.playerId != 'B')) return INVALID_PLAYER;
	else playerturn = move.playerId;
	*/
	if (playerturn != move.playerId ) return INVALID_PLAYER;
	return 0;
}

int TickTacToe::restart()
{
	board.clearBoard('.');
	playerturn = 'A';
	movesLeft = 9;
	return 0;
}

int TickTacToe::makeMove(PlayerMove move)
{
	int result;
	if (result = verifyMove(move)) return result;
	if (result = board.set(move)) return result;
	if (playerturn == 'A') playerturn = 'B'; else playerturn = 'A';
	movesLeft--;
	if (result = checkVictory()) movesLeft = 0;
	return result;
}

char* TickTacToe::getGameState()
{
	return board.getBoard();
}
