#pragma once
// Defines msg codes commonly used in this lib as well as Board3x3 and PlayerMove structs

#define STATE_REQUESTED -1
#define INPROGRESS 0
#define INVALID_REQUEST 1
#define INVALID_PLAYER 2
#define OUT_OF_RANGE 3
#define ALREADY_OCCUPIED 4
#define PLAYER1_WON 10
#define PLAYER2_WON 11
#define UNDECIDED 12

struct Board3x3
{
	char x[3][3];
};

// char playerId, int row, int column
struct PlayerMove
{
	char playerId;
	unsigned int row;
	unsigned int column;
};