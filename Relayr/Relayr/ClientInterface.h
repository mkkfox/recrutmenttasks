#pragma once
// Defines basic communication interface for client apps:
// msgSend and msgSend to should provide two way communication and return response msg
// disconect should provide new initialized socket in work ready state
// getAddr should convert doted IP address and port number to sockAddr

#include <string>
#include <WinSock2.h>
#include <Ws2tcpip.h>
#include "NetworkDefines.h"

class ClientInterface
{
public:
	ClientInterface();
	virtual	~ClientInterface();

	// Send msg to connected server
	virtual std::string msgSend(std::string) = 0;

	// Send msg to specified server
	virtual std::string msgSendTo(std::string, sockaddr *) = 0;

	// Connect to specified server
	virtual int connectTo(const sockaddr *) = 0;

	// Close current socket and create new socket
	virtual int disconect() = 0;

	// Get connected server sockaddr
	virtual sockaddr getConnectedServerAddr()const = 0;

	// Get Addr
	virtual sockaddr * getAddr(const char* ip, const int port) = 0;
};

