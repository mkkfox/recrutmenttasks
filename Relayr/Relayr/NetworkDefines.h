#pragma once
// Defines default network values used in this lib.

#define DEFAULT_PORT 27015
#define DEFAULT_IP "127.0.0.1"
#define BUFF_SIZE 512