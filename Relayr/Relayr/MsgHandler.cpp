#include "MsgHandler.h"

MsgHandler::MsgHandler()
{
}

MsgHandler::~MsgHandler()
{
}

std::string MsgHandler::toMsg()
{
	json Msg;
	Msg["response"] = "ok";
	return Msg.dump();
}

std::string MsgHandler::toMsg(int err)
{
	json Msg;
	Msg["response"] = "error";
	Msg["error_code"] = err;
	return Msg.dump();
}

std::string MsgHandler::toMsg(char * gameBoard)
{
	json Msg;
	std::string board = gameBoard;
	std::string rows[3];
	for (int i = 0; i < 3; i++) rows[i] = board.substr(i * 3, 3);
	Msg["response"] = "game status";
	Msg["board"] = { rows[0], rows[1], rows[2] };
	//delete[] gameBoard; // Might couse heap corruption, delete[] in parent object
	return Msg.dump();
}

std::string MsgHandler::toMsg(std::string winner)
{
	json Msg;
	Msg["response"] = "game over";
	Msg["details"] = winner;
	return Msg.dump();
}

json MsgHandler::toRequest(std::string msg)
{
	return json::parse(msg);
}
