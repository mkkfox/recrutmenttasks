#include "ClientUDP.h"

int ClientUDP::initialize()
{
	UDPSocket = INVALID_SOCKET;
	WSADATA wsaData;
	int err;
	if (err = WSAStartup(MAKEWORD(2, 2), &wsaData))
	{
		printf("WSAStartup error: %d\n", err);
		return -1;
	}
	return 0;
}

ClientUDP::ClientUDP()// TODO: Add error checking
{
	ZeroMemory(&connectedServerAddr,sizeof(connectedServerAddr));
	initialize();
	UDPSocket = socket(AF_INET, SOCK_DGRAM, 0);
}

ClientUDP::ClientUDP(const sockaddr *serverAddr)// TODO: Add error checking
{	
	ZeroMemory(&connectedServerAddr, sizeof(connectedServerAddr));
	initialize();
	UDPSocket = socket(AF_INET, SOCK_DGRAM, 0);
	connectTo(serverAddr);
}

ClientUDP::~ClientUDP()
{
	closesocket(UDPSocket);
	WSACleanup();
}

std::string ClientUDP::msgSend(std::string msg)// TODO: Add error checking
{
	if (connectedServerAddr.sa_data == 0) return std::string();
	send(UDPSocket, msg.c_str(), msg.size(),0);
	int size = sizeof(connectedServerAddr);
	int r = recvfrom(UDPSocket, received, sizeof(received), 0, &connectedServerAddr, &size);
	if (r > 0)
	{
		received[r] = '\0';
		return std::string(received);
	}
	return std::string();
}

std::string ClientUDP::msgSendTo(std::string msg, sockaddr * serverAddr)// TODO: Add error checking
{
	int size = sizeof(serverAddr);
	sendto(UDPSocket, msg.c_str(), msg.size(), 0, serverAddr, sizeof(*serverAddr));
	int r  = recvfrom(UDPSocket, received, sizeof(received), 0, serverAddr, &size);
	if (r > 0)
	{
		received[r] = '\0';
		return std::string(received);
	}
	return std::string();
}

int ClientUDP::connectTo(const sockaddr * serverAddr)// TODO: Add error checking
{
	connect(UDPSocket, serverAddr, sizeof(*serverAddr));
	memcpy(this->connectedServerAddr.sa_data, serverAddr->sa_data, sizeof(*serverAddr));
	this->connectedServerAddr.sa_family = serverAddr->sa_family;
	return 0;
}

int ClientUDP::disconect()
{
	closesocket(UDPSocket);
	UDPSocket = socket(AF_INET, SOCK_DGRAM, 0);
	connectedServerAddr = { 0 };
	return 0;
}

sockaddr ClientUDP::getConnectedServerAddr() const 
{
	return connectedServerAddr;
}

sockaddr * ClientUDP::getAddr(const char * ip,const int port)
{
	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	InetPton(AF_INET, ip, &serveraddr.sin_addr);
	serveraddr.sin_port = htons(port);
	return (sockaddr*)&serveraddr;
}
