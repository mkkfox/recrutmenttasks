#pragma once
// Message serialization/deserialization for server apps.
// Builds responses using overloaded function toMsg().
// If needed toRequest() provides serialized msg parse to JSON object.

#include "MsgHandlerInterface.h"

class MsgHandler :
	public MsgHandlerInterface
{
public:
	MsgHandler();
	~MsgHandler();
	// Serialize {"response": "ok"} 
	virtual std::string toMsg() override;

	// Serialize "response": "error", "error_code": err
	virtual std::string toMsg(int err) override;

	// Serialize "response": "game status", "board": ["row1", "row2", "row3"]
	virtual std::string toMsg(char* gameBoard) override;

	// Serialize "response": "game over", "details": winner
	virtual std::string toMsg(std::string winner) override;

	// Parse msg to JSON object (deserialize)
	virtual json toRequest(std::string msg) override;
};

