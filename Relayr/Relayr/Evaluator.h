#pragma once
// Provides simple evaluation of victory conditions for Tick Tac Toe game

#include <list>
#include "GameDefines.h"

struct WinLine
{
	char *f1;
	char *f2;
	char *f3;
};

class Evaluator
{
public:
	Evaluator();
	Evaluator(Board3x3 *board);
	~Evaluator();
private:
	std::list<WinLine> lines;	// List of game winning configurations
	Board3x3 *target;			// Connected game board
	int build();				// Populate lines list
public:
	// Build winlines list for specified game board
	int setTarget(Board3x3 *board);

	// Check if any line is complete
	char check();
};

