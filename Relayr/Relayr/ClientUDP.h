#pragma once
// Basic Com class for client apps provides two way UDP socket based communication.
// Use getAddr to convert doted IP address and port number to sockAddr.

#include "ClientInterface.h"

class ClientUDP :
	public ClientInterface
{
protected:
	int initialize();			// Initialize WinSock
	char received[BUFF_SIZE];	// Buffor for incoming data
public:
	ClientUDP();
	ClientUDP(const sockaddr *serverAddr);
	virtual ~ClientUDP();

	// Send msg to connected server
	virtual std::string msgSend(std::string msg) override;

	// Send msg to specified server
	virtual std::string msgSendTo(std::string msg, sockaddr * serverAddr) override;

	// Connect to specified server
	virtual int connectTo(const sockaddr *serverAddr) override;

	// Close current socket and create new socket
	virtual int disconect() override;

	// Get connected server sockaddr
	virtual sockaddr getConnectedServerAddr() const override;

	// Get Addr struct
	virtual sockaddr * getAddr(const char* ip, const int port) override;
protected:
	SOCKET UDPSocket;				// Socket descriptor
	sockaddr connectedServerAddr;	// Conected server
	SOCKADDR_IN serveraddr;			// Used for getAddr
};

